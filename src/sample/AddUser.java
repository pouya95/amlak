package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class AddUser {
    @FXML
    public TextField usertxt;
    @FXML
    public TextField passtxt;



    public void AddUser(ActionEvent actionEvent) {
        try {
            String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=amlak;user=sa;password=47611";
            Connection con = null;
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
            String sql2 = "exec addUser ?,?";
            PreparedStatement ss = con.prepareStatement(sql2);
            ss.setString(1, usertxt.getText());
            ss.setString(2, passtxt.getText());
            ss.executeQuery();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
