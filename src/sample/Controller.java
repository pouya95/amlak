package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.*;

public class Controller {
    @FXML
    public Button bt1;
    @FXML
    public VBox vb1;
    @FXML
    public TextField user1;
    @FXML
    public PasswordField pass1;
    @FXML
    public AnchorPane pn2;


    public void click(ActionEvent actionEvent) {
        try {

            String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=amlak;user=sa;password=47611";
            Connection con = null;
            Statement stmt = null;
            ResultSet rs = null;

            // Establish the connection.
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);


            String sql1 = "select * from Users where username=? and password=?";

            PreparedStatement ss = con.prepareStatement(sql1);
            ss.setString(1, user1.getText());
            ss.setString(2, pass1.getText());

            rs = ss.executeQuery();


            if(rs.next()) {
                Parent p=FXMLLoader.load(Main.class.getResource("menu.fxml"));
                vb1.setDisable(true);
                Stage stage =new Stage();
                Scene scene =new Scene(p);
                stage.setScene(scene);
                stage.show();

            }


        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}









